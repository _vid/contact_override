# Contact Override: Use hook_mail_alter to add the sender's email address to the body of the email.

## Contact

### Current maintainers:

* Vid Rowan ([_vid](http://drupal.org/user/631512/))

## Drupal Version

* Drupal 6

## Purpose
The contact_override module is a simple custom module that adds the senders email to the body of the contact page message. For example, the standard email message would be appended with this:
> --
> Mail sent out from Drupal on behalf of vid@test-site.test.

* Note: 'Drupal' is substituted with the site_name if set.

## Demonstrated Need

* It was noticed that while emails from the Drupal 6 contact form contained the senders email in the reply field that email address was often lost when forwarding the message.  
For example when an email is received from the 'Test Site' contact form the reply line may look like this:
> From: test@test-site.test on behalf of Vid < vid@test-site.test >  

    When that message is forwarded the email address in the brackets is lost in the body of the message, like so:
> -----Original Message-----
> From: test@test-site.test [mailto:test@test-site.test] On Behalf Of Vid

    This module adds that line to the message body to work around this issue. So now we would see this at the end of the email:
> --
> Mail sent out from Test Site on behalf of vid@test-site.test.

## Scope: What this module does

* Uses hook_mail_alter to add content to the body of the email

## Out of Scope: What this module does not do

* This module does not have an admin settings page

## Making Changes

*   Edits that would be made in the .module file
    For example: to include more mail types, you would add a new case to the $messages['id'] switch statement. To get the messages id check your watchdog log for 'contact_override' entries.
*   If you do make changes consider a pull request

## Requirements

* None

## References

* Ref: http://api.drupal.org/api/drupal/includes!mail.inc/function/drupal_mail/6
* Ref: http://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_mail_alter/6

## Repositories

* bitbucket.com
    * General info: https://bitbucket.org/_vid/contact_override
    * Clone ssh: git@bitbucket.org:_vid/contact_override.git
    * Clone http: https://_vid@bitbucket.org/_vid/contact_override.git
* git.uoregon.edu
    * General info: https://git.uoregon.edu/projects/UO_DRPL_PROD/repos/contact_override/browse
    * Clone ssh: ssh://git@git.uoregon.edu/UO_DRPL_PROD/contact_override.git
    * Clone http: http://vid@git.uoregon.edu/scm/UO_DRPL_PROD/contact_override.git

## Installation

* Install module as usual, see [Installing contributed modules](http://drupal.org/node/70151
  for further information.

## Usage

### In short:
* Enable the module. Once enabled the body of the contact page template will be modified.

## Git.uoregon.edu Repo Notes:
* The master branch can only be written to by Vid.
* Feel free to create new branches and push them up. The dev branch can be written to by anyone.
